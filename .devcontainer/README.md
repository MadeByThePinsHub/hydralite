# Dev Container Docs

This directory hosts the files and configuration for Gitpod workspace image building, container image building in each components (in individual component's Dockerfile) and
for the Dev Container Remote VS Code extension in the future.

## Contents

- `flutter.bashrc` - Basically custom bashrc file for Flutter inside Gitpod workspaces. Not necessarily used for the main VS Code Dev Container stuff.
