# Welcome to Hydralite

<p>
  <img src="https://img.shields.io/badge/version-1.0.0--pre--alpha-ff69b4"> <img src="https://img.shields.io/tokei/lines/github/hydralite/hydralite?color=white&label=lines%20of%20code"> <img src="https://img.shields.io/github/languages/top/hydralite/hydralite?color=%230xfffff"> <img src="https://img.shields.io/github/repo-size/hydralite/hydralite?color=orange">
</p>

## What Is Hydralite?

Hydralite is an open-source, next-generation project marketing and management platform that uses a platform-specific "currency" called Hydra, which enables you to build a vast community around your product using an algorithmic, promotional feed of content, as well as to conduct your software effectively with an intuitive, one-of-a-kind product management framework.

## File Structure

<a href="https://github.com/hydralite/hydralite/tree/dev/web">`web`</a> - Web App <br>
<a href="https://github.com/hydralite/hydralite/tree/dev/api">`api`</a> - GraphQL Api <br>
<a href="https://github.com/hydralite/hydralite/tree/dev/cli">`cli`</a> - CLI <br>
<a href="https://github.com/hydralite/hydralite/tree/dev/bot">`hydrabot`</a> - Hydralite Discord Bot <br>
<a href="https://github.com/hydralite/hydralite/tree/dev/landing">`landing`</a> - Landing Page <br>
<a href="https://github.com/hydralite/hydralite/tree/dev/mobile">`mobile`</a> - Mobile <br>
<a href="https://github.com/hydralite/prototypes">`repo:prototypes`</a> - Prototypes

## Quick Start

Although we highly recommend reading <a href="https://github.com/hydralite/hydralite/blob/dev/CONTRIBUTING.md">contributing.md</a>, here's the fastest way to get started.

Make sure you've installed Node.js (Web, API, landing, Discord bot), Python (automation), Rust (CLI) and/or Flutter + Dart (Mobile App) before start hacking/contributing within your local machine, especially if you use [code-server](https://github.com/cdr/code-server)

To start both the web and api servers -

1. Navigate to the root directory and install dependencies with `yarn` (or `yarn install`)
2. Then, run an intuitive setup procedure with `yarn setup`
3. Once Setup is complete, run `yarn dev`

**Other Ways To Get Started**

- Running Hydralite With Docker - <a href="https://github.com/hydralite/hydralite/blob/dev/CONTRIBUTING.md#using-docker">Click Here</a>
- Running Hydralite On Gitpod - <a href="https://github.com/hydralite/hydralite/blob/dev/CONTRIBUTING.md#using-gitpod">Click Here</a>
